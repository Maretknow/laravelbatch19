<?php 
trait hewan{
    public $nama;
    public $darah = 50;
    public $jumlahKaki;
    public $keahlian;

    public function atraksi() {
        $string = $this->nama. ' sedang ' .$this->keahlian;
        return $string;
    }

}

trait fight{
    public $attackPower;
    public $defencePower;
    
    public function serang($lawan) {
        $string = $this->nama. ' menyerang ' .$lawan->nama;
        return $string;
    }
    
    public function diserang($penyerang) {
        $this->darah = $this->darah - $penyerang->attackPower/$this->defencePower;
        $string = $this->nama. ' Sedang Di Serang ' .$penyerang->nama. ' saat ini darahnya ' .$this->nama. ' nya adalah ' .$this->darah ;
        return $string;
    }
}

class elang{
    use hewan, fight;
    function __construct($nama, $jumlahKaki, $keahlian, $attackPower, $defencePower) {
        $this->nama = $nama;
        $this->jumlahKaki = $jumlahKaki;
        $this->keahlian = $keahlian;
        $this->attackPower = $attackPower;
        $this->defencePower = $defencePower;

    }

    public function getInfoHewan(){
        $string = 'Nama Hewan adalah ' .$this->nama. ' memiliki HP = ' .$this->darah. ' dengan 
        jumlah kaki ' .$this->jumlahKaki. ' dan keahlian '.$this->keahlian. ' yang memilik 
        kekuatan ' .$this->attackPower. ' dan pertahanan sejumlah ' .$this->defencePower;
        
        return $string;
    }
}

class harimau{
    use hewan, fight;
    function __construct($nama, $jumlahKaki, $keahlian, $attackPower, $defencePower) {
        $this->nama = $nama;
        $this->jumlahKaki = $jumlahKaki;
        $this->keahlian = $keahlian;
        $this->attackPower = $attackPower;
        $this->defencePower = $defencePower;

    }

    public function getInfoHewan(){
        $string = 'Nama Hewan adalah ' .$this->nama. ' memiliki HP = ' .$this->darah. ' dengan 
        jumlah kaki ' .$this->jumlahKaki. ' dan keahlian '.$this->keahlian. ' yang memilik 
        kekuatan ' .$this->attackPower. ' dan pertahanan sejumlah ' .$this->defencePower;
        
        return $string;
    }
}

$elang = new elang('Elang_1', 2 , 'Terbang Tinggi', 10, 5);

$harimau = new harimau('Harimau_1', 4 , 'Lari Cepat', 7, 8);
echo $harimau->atraksi();
echo '</br>';
echo $harimau->serang($elang);
echo '</br>';
echo $harimau->diserang($elang);
echo '</br>';
echo $harimau->getInfoHewan();

echo '</br>';
echo '</br>';

echo $elang->atraksi();
echo '</br>';
echo $elang->serang($harimau);
echo '</br>';
echo $elang->diserang($harimau);
echo '</br>';
echo $elang->getInfoHewan();
echo '</br>';


?>