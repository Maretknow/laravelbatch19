<?php

namespace App;

use App\Traits\Uuid;
use Illuminate\Database\Eloquent\Model;

class Blog extends Model
{
    use Uuid;

    protected $guarded = [];
}
