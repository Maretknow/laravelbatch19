<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use App\User;

class UserRegisteredMail extends Mailable
{
    use Queueable, SerializesModels;

    protected $user;
    protected $pesan;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($user, $pesan)
    {
        $this->user = $user;
        $this->pesan = $pesan;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('example@example.com')
            ->view('send_email_notification_user_register')
            ->with([
                'name' => $this->user->name,
                'otp_code' => $this->user->otp_code->otp_name,
                'pesan' => $this->pesan,
            ]);
    }
}
