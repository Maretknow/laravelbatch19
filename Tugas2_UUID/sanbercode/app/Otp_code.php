<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;
use App\Traits\Uuid;
use Carbon\Carbon;

class Otp_code extends Model
{
    use Uuid;
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id','otp_name','user_id', 'valid_until'
    ];


    /* 
    * get the user record with otp
    */
    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
