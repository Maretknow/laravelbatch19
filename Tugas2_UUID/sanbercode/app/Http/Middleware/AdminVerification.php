<?php

namespace App\Http\Middleware;
use Illuminate\Support\Facades\Auth;
use Closure;
use App\Role;

class AdminVerification
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $id_admin = Role::where('name','Admin')->first();
        if(Auth::user()->role->id == $id_admin->id){
            return $next($request);
        }
        return redirect('/emailverified');
    }
}
