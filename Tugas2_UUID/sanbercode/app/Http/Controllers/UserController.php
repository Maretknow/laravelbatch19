<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class UserController extends Controller
{

    public function __construct(){
        //$this->middleware('auth');
    }
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {   
        if(!$request->user()) {
            return response()->json([
            'response_code' => '01',
            'response_message' => 'Unautenticated'
            ]);
        }

        return response()->json([
            'response_code' => '00',
            'response_message' => 'Profile berhasil di tampilkan',
            'data' => ['profile' => $request->user()]
        ]);
    }
}
