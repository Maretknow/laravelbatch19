<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class ProfileController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {

        $file = $request->file('photo');
		$nama_file = 'upload/user/'.time()."_".$file->getClientOriginalName();
		$tujuan_upload = 'upload/user ';
		$file->move($tujuan_upload,$nama_file);


        $user = User::firstWhere('id',$request->user()->id);
        $user->name = $request->name;
        $user->photo = $nama_file;
        $user->update();

        return response()->json([
            'response_code' => '00',
            'response_message' => 'Profile berhasil di update',
            'data' => ['profile' => $user]
        ]);
    }
}
