<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $request->validate([
            'email' => 'required',
            'password' => 'required'
        ]);

        if (!$token = auth()->attempt($request->only('email', 'password'), ['email_verified_at' => NULL])) {
            return response(['message' => 'Gagal Login, cek email & password'], 401);
        };

        return response()->json([
            'response_code' => '00',
            'response_message' => 'Anda Berhasil Login',
            'data' => [
                'token' => $token,
                'user' =>  $request->user(),
            ]
        ]);
    }
}
