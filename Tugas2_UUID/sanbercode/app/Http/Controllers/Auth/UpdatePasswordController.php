<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Hash;

class UpdatePasswordController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $request->validate([
            'email' => 'required',
            'password' => 'required|confirmed|min:6'
        ]);


        $user = User::firstWhere('email', $request->email);

        if (!$user->email_verified_at) {
            return 'silahkan verivikasi email dahulu';
        } else {
            $user->password = Hash::make($request['password']);
            $user->save();


            return response()->json([
                'response_code' => '00',
                'response_message' => 'Password berhasil di update',
                'data' => ['user' => $user]
            ]);
        }
    }
}
