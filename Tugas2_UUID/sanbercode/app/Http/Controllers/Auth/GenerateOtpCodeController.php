<?php

namespace App\Http\Controllers\Auth;

use App\Events\UserRegisteredEvent;
use App\Http\Controllers\Controller;
use App\Mail\UserRegisteredMail;
use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Str;
use Carbon\Carbon;
use Mail;

class GenerateOtpCodeController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $user = User::firstWhere('email', $request->email);

        //return $user;

        $now = Carbon::now();
        $valid_until = $now->addMinute(5);

        $user->otp_code()->update([
            'otp_name' => Str::random(5),
            'valid_until' => $valid_until
        ]);

        $user->pesan = "Kode Anda Telah Kami Generate, Masukan kode otp untuk melanjutkan";
        event(new UserRegisteredEvent($user));

        return response()->json([
            'response_code' => '00',
            'response_message' => 'Kode Anda Telah di generate ulang, Silahkan Cek email Kembali',
            'data' => ['user' => $user]
        ]);
    }
}
