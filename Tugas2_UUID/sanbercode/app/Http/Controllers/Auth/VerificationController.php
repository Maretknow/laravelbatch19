<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Otp_code;
use Illuminate\Http\Request;
use Carbon\Carbon;

class VerificationController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $otp_code = Otp_code::firstWhere('otp_name', $request->otp_name);

        if (!$otp_code) {
            return response()->json([
                "response_code" => '01',
                'response_message' => 'OTP Code tidak ditemukan'
            ]);
        } else {

            $valid = $otp_code->valid_until;
            $current = Carbon::now();
            $now = $current->toDateTimeString();

            if ($now >= $valid) {
                return response()->json([
                    "response_code" => '01',
                    'response_message' => 'OTP Code Sudah Kadaluaras, Silahkan generate ulang'
                ]);
            } else {
                $otp_code->user()->update([
                    'email_verified_at' => $now
                ]);

                return response()->json([
                    'response_code' => '00',
                    'response_message' => 'Berhasil Diverifikasi',
                    'data' => ['user' => $otp_code->user()->get()]
                ]);
            }
        }
    }
}
