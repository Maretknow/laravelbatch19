<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });



Route::namespace('Auth')->group(function() {
    Route::post('/auth/register', 'RegisterController');
    Route::post('/auth/verification', 'VerificationController');
    Route::post('/auth/regenerate_otp', 'GenerateOtpCodeController');
    Route::post('/auth/update_password', 'UpdatePasswordController');
    Route::post('/auth/login', 'LoginController');
    Route::post('logout', 'LogoutController');
});

Route::get('user', 'UserController');
Route::post('/profile/update_profile', 'ProfileController');

Route::group([
    'middleware' => 'api',
    'prefix' => 'campaign',
], function() {
    Route::get('random/{count}', 'CampaignController@random');
    Route::post('store', 'CampaignController@store');
    Route::get('/', 'CampaignController@index');
    Route::get('/{id}', 'CampaignController@detail');
});

Route::group([
    'middleware' => 'api',
    'prefix' => 'blog',
], function() {
    Route::get('random/{count}', 'BlogController@random');
    Route::post('store', 'BlogController@store');
});

