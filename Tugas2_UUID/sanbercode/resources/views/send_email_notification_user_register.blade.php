<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title></title>
</head>

<body>
    <p>Selamat Anda <b>{{ $name }}</b> telah berhasil terdaftar</p>
    @if($pesan !== null)
    {{ $pesan }}
    @endif
    <p>Kode OTP anda adalah <b>{{ $otp_code }}</b>, Kode ini sangat rahasia ya.</p>
</body>

</html>